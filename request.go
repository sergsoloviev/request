package request

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/google/uuid"
)

type Request struct {
	Id           uuid.UUID
	Url          string
	Method       string
	Body         []byte
	ResponseBody []byte
	Client       http.Client
	Request      *http.Request
	Response     *http.Response
	Timeout      time.Duration
	Logging      bool
}

func (o *Request) SetHeader(key string, value string) {
	o.Request.Header.Set(key, value)
}

func (o *Request) Make() (err error) {
	o.Id = uuid.New()
	o.Request, err = http.NewRequest(o.Method, o.Url, bytes.NewBuffer(o.Body))
	if err != nil {
		log.Println(err)
		return err
	}
	o.SetHeader("Content-Type", "application/json")
	o.SetHeader("X-Request-Id", fmt.Sprintf("%s", o.Id))
	if o.Logging {
		var reqHeaders string
		for k, v := range o.Request.Header {
			reqHeaders += fmt.Sprintf("%s: %s\n", k, v[0])
		}
		log.Printf("[%s] Request: %s %s\n%s\n%s", o.Id, o.Method, o.Url, reqHeaders, o.Body)
	}
	o.Client = http.Client{}
	o.Response, err = o.Client.Do(o.Request)
	if err != nil {
		log.Println(err)
		return err
	}
	o.ResponseBody, err = ioutil.ReadAll(o.Response.Body)
	defer o.Response.Body.Close()
	if err != nil {
		log.Println(err)
		return err
	}
	if o.Logging {
		var respHeaders string
		for k, v := range o.Response.Header {
			respHeaders += fmt.Sprintf("%s: %s\n", k, v[0])
		}
		log.Printf("[%s] Response: %d\n%s\n%s", o.Id, o.Response.StatusCode, respHeaders, o.ResponseBody)
	}
	return nil
}
