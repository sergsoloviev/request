# package request

```go
package main

import (
	"bitbucket.org/sergsoloviev/request"
)

func main() {
	req := request.Request{
		Url:     "http://httpbin.org/post",
		Body:    []byte("{\"a\":1}"),
		Method:  "POST",
		Logging: true,
	}
	req.Make()
}
```
```bash
bash-3.2$ go run main.go
2019/03/15 16:00:20 [45030767-3738-4c5b-a14b-9ce5a560f807] Request: POST http://httpbin.org/post
Content-Type: application/json
X-Request-Id: 45030767-3738-4c5b-a14b-9ce5a560f807

{"a":1}
2019/03/15 16:00:21 [45030767-3738-4c5b-a14b-9ce5a560f807] Response: 200
Content-Type: application/json
Date: Fri, 15 Mar 2019 13:00:21 GMT
Server: nginx
Connection: keep-alive
Access-Control-Allow-Credentials: true
Access-Control-Allow-Origin: *

{
  "args": {}, 
  "data": "{\"a\":1}", 
  "files": {}, 
  "form": {}, 
  "headers": {
    "Accept-Encoding": "gzip", 
    "Content-Length": "7", 
    "Content-Type": "application/json", 
    "Host": "httpbin.org", 
    "User-Agent": "Go-http-client/1.1"
  }, 
  "json": {
    "a": 1
  }, 
  "origin": "12.34.56.78, 12.34.56.78", 
  "url": "https://httpbin.org/post"
}
```
